## Desafio Processo Seletivo Estoque Parado 2021

### Objetivos
O objetivo deste desafio é avaliar como o candidato traduz um problema apresentado em código, utilizando de seus conhecimentos em lógica de programação.
Além disso, esse desafio estimula e avalia a busca de soluções e exemplos em conteúdos já disponíveis na internet, habilidade essencial para qualquer desenvolvedor.

### Como funciona o desafio?
- O código desenvolvido deve ser entregue em um repositório público do github. O link do repositório deve ser enviado por email para flavio@estoqueparado.com.br.
- O repositório deve possuir um arquivo README.md, com um manual simples de como o projeto pode ser construído, executado e como ser usado. O manual deve incluir o que precisa ser instalado no computador para que o código seja executado.
- O prazo de entrega será informado via email.
- Procure resolver todos os problemas, **porém o desafio pode ser entregue de forma incompleta**. Iremos analisar todas as entregas, mesmo que parciais ou não funcionando corretamente. Se julgar necessário, inclua no README ou comente no código o que foi desenvolvido, o que faltou, e quais as dificuldades. Para nós o importante é entender como o problema foi recebido, interpretado e quais as alternativas encontradas para tentar solucioná-lo.
- O desafio pode ser feito utilizando qualquer linguagem de programação de sua preferência desde que a mesma possa ser executada no **Linux**.

## Desafio
#### Introdução
API (do inglês application programming interface) é basicamente uma interface desenvolvida em um software que permite a comunicação entre sistemas diferentes usando um padrão pré-estabelecido. Um desses padrões é o REST. De forma simplificada, o padrão REST estabelece uma forma de comunicação entre sistemas utilizando o protocolo [HTTP](https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Overview) e seus recursos, para trafegar dados no formato [json](https://www.json.org/json-pt.html).

Para a resolução do desafio, você utilizará uma API Rest como fonte de dados.
Em caso de dificuldades com as documentações em inglês, utilize o [Google tradutor](https://translate.google.com.br/?hl=pt-BR). Cole o link do site no campo de idioma de origem, selecione português para a tradução e ele irá gerar um link para o site traduzido.
### SWAPI The Star Wars API
A api que iremos utilizar é a [SWAPI](https://swapi.dev/). Ela é um repositório de informações sobre o universo de Star Wars que disponibiliza uma API Rest em que podemos consultar informações sobre filmes, personagens, naves, etc.
Na página inicial do projeto você encontrará uma descrição de alguns desses serviços e um formulário onde você pode testar as chamadas para a API no próprio site.
[Nesta página](https://swapi.dev/documentation) Você encontra a documentação completa de todos os recursos da API. Por ser uma api gratuita, existe um limite diário de 10 mil requisições.
**Atenção!** Os dados retornados pela api são paginados, ou seja, a chamada inicial pode retornar somente alguns registros, indicando no corpo da resposta onde está a próxima página com o restante do conteúdo. Alguns recursos podem possuir uma ou mais páginas.
Se tiver dúvidas de como consumir uma API Rest, os links abaixo podem dar uma direção:
1. [https://arthur-almeida.medium.com/consumindo-uma-api-de-maneira-simples-com-java-2a386010e4b9](https://arthur-almeida.medium.com/consumindo-uma-api-de-maneira-simples-com-java-2a386010e4b9)
2. [https://www.canalti.com.br/programacao/web/php/como-consumir-api-com-php-pokemons-listar-dados-file_get_contents-e-curl/](https://www.canalti.com.br/programacao/web/php/como-consumir-api-com-php-pokemons-listar-dados-file_get_contents-e-curl/)
### Exercício:
1.  Implemente um método que retorne um array com o nome e ano de nascimento de todos os personagens de Star Wars provenientes da SWAPI que possuem em seu nome a letra **e**.
2. Implemente um método que retorne um array com o nome e o diretor de todos os filmes dos quais o personagem **Luke Skywalker** aparece.
3. Implemente um método que retorne um array com o nome, modelo e o fabricante (manufacturer) de todas as naves (starships) presentes em todos os filmes dos quais o personagem **Darth Vader** aparece.
4. Implemente uma página html que mostre em uma tabela os resultados retornados pelo método implementado no exercício 1.
5. Implemente uma página html que mostre em uma tabela os resultados retornados pelo método implementado no exercício 2.
6. Implemente uma página html que mostre em uma tabela os resultados retornados pelo método implementado no exercício 3.
7. Implemente uma página html que possua 3 links, cada um redirecionando o usuário para uma das páginas criadas nos exercícios 4, 5 e 6. Essa será a página inicial do usuário.