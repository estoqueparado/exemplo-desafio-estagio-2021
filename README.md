## Exemplo de resolução do [desafio do processo Estoque Parado 2021](DESAFIO.md)

### Pré-requisitos
Para rodar o projeto, as dependências a seguir devem ser instaladas:

- [JDK 8 (ou superior)](https://digitalinnovation.one/artigos/como-instalar-o-java-com-o-apt-no-ubuntu-2004#:~:text=A%20op%C3%A7%C3%A3o%20mais%20f%C3%A1cil%20para,atualize%20o%20%C3%ADndice%20de%20pacotes.&text=O%20JRE%20permitir%C3%A1%20que%20voc%C3%AA%20execute%20quase%20todos%20os%20softwares%20Java.)
- [Maven 3](https://www.hostinger.com.br/tutoriais/install-maven-ubuntu)
- [Git](https://git-scm.com/book/pt-br/v2/Come%C3%A7ando-Instalando-o-Git)
- Para visualizar o código, um editor de código ou IDE de sua preferência (IntelliJ, Eclipse, VS Code, etc.).

### Tecnologias utilizadas
- [Java 8](https://www.java.com/pt-BR/download/help/java8_pt-br.html)
- [Maven](https://maven.apache.org/)
- [Spring boot](https://spring.io/projects/spring-boot)
- [Bootstrap](https://getbootstrap.com/)
- [IntelliJ](https://www.jetbrains.com/pt-br/idea/)
- [Spring boot initializer](https://start.spring.io/)
- [SWAPI Rest API](https://swapi.dev/)
- [Git](https://git-scm.com/)

### Como rodar o projeto

- Clone este repositório usando git clone.
- No terminal, entre no diretório onde o projeto foi clonado e execute `mvn spring-boot:run`
- Ao ser inicializado, você verá uma tela similar a imagem abaixo:
![Sping boot startup](readme-resources/spring-boot-startup.png)
- Em um navegador, acesse o endereço `localhost:8080`
- A página inicial do projeto deverá ser exibida.
![Página inicial](readme-resources/home-page.png)
- Em caso de problemas, entre em contato em flavio@estoqueparado.com.br

### Implementações importantes:

[SWAPIService](src/main/java/com/estoqueparado/desafioestagio/services/SWAPIService.java) - Classe que consome a API e implementa as regras dos exercícios.

[ViewsController](src/main/java/com/estoqueparado/desafioestagio/controllers/ViewsController.java) - Classe que faz o mapeamento das url's do frontend.
