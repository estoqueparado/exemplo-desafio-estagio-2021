<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
    <head>
        <title>Resolução do Exercício 4</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container-fluid">
            <h3>Resolução do Exercício 4:</h3>
            <p>Implemente uma página html que mostre em uma tabela os resultados retornados pelo método implementado no exercício 1.</p>
            <div class="row">
                <div class="col-sm-2 col-sm-offset-4">
                    <span>Nome</span>
                </div>
                <div class="col-sm-2">
                    <span>Ano de Nascimento</span>
                </div>
            </div>
            <c:forEach var="p" items="${people}" varStatus="loop">
                <div class="row">
                    <div class="col-sm-2 col-sm-offset-4" <c:if test="${loop.index % 2 == 0}">style="background-color: #dcdcdc;"</c:if>>
                        <span><c:out value="${p.name}"/></span>
                    </div>
                    <div class="col-sm-2" <c:if test="${loop.index % 2 == 0}">style="background-color: #dcdcdc;"</c:if>>
                        <span><c:out value="${p.birthYear}"/></span>
                    </div>
                </div>
            </c:forEach>
        </div>
    </body>
</html>