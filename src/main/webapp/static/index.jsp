<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4" align="center">
                    <img src="https://www.estoqueparado.com.br/media/logo/stores/1/logo-normal.png"/>
                    <h2>Desafio Est�gio Estoque Parado</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4" align="center">
                    <a href="/exercise4">Exerc�cio 4 (Resultados do exerc�cio 1)</a>
                </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4" align="center">
                    <a href="/exercise5">Exerc�cio 5 (Resultados do exerc�cio 2)</a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4" align="center">
                    <a href="/exercise6">Exerc�cio 6 (Resultados do exerc�cio 3)</a>
                </div>
            </div>
        </div>
    </body>
</html>