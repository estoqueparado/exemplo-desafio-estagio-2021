package com.estoqueparado.desafioestagio.services;

import com.estoqueparado.desafioestagio.models.Film;
import com.estoqueparado.desafioestagio.models.People;
import com.estoqueparado.desafioestagio.models.PeoplePageResult;
import com.estoqueparado.desafioestagio.models.Starship;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SWAPIService {

    private final RestTemplate restTemplate = new RestTemplate();

    /*
     * Exercise 1 resolution
     */
    public List<People> getResultExercise1(){

        List<People> result = new ArrayList<>();

        String apiUrl = "https://swapi.dev/api/people";
        boolean hasNext = true;

        while(hasNext){

            ResponseEntity<PeoplePageResult> apiResult = restTemplate.getForEntity(apiUrl, PeoplePageResult.class);
            result.addAll(apiResult.getBody().getResults());

            if(apiResult.getBody().getNext() == null){
                hasNext = false;
            }else{
                apiUrl = apiResult.getBody().getNext();
            }
        }

        List<People> filteredResult = new ArrayList<>();

        for (People people : result){
            if(people.getName().contains("e") || people.getName().contains("E")){
                filteredResult.add(people);
            }
        }

        return filteredResult;
    }

    /*
     * Exercise 2 resolution
     */
    public List<Film> getResultExercise2(){

        //api url to get Luke Skywalker = https://swapi.dev/api/people/1

        People lukeSkywalker = restTemplate.getForObject("https://swapi.dev/api/people/1", People.class);

        List<Film> result = new ArrayList<>();

        for (String filmUrl : lukeSkywalker.getFilms()){

            Film film = restTemplate.getForObject(filmUrl, Film.class);
            result.add(film);
        }

        return result;
    }

    /*
     * Exercise 3 resolution
     */
    public List<Starship> getResultExercise3(){

        //api url to get Darth Vader = https://swapi.dev/api/people/4/

        People darthVader = restTemplate.getForObject("https://swapi.dev/api/people/4/", People.class);

        List<Film> films = new ArrayList<>();

        for (String filmUrl : darthVader.getFilms()){
            Film film = restTemplate.getForObject(filmUrl, Film.class);
            films.add(film);
        }


        List<Starship> starships = new ArrayList<>();
        //use as control to avoid duplicated starships
        List<String> starshipsUrls = new ArrayList<>();

        for(Film film : films){
            for (String starshipUrl : film.getStarships()){

                if(starshipsUrls.contains(starshipUrl)){
                    continue;
                }

                starshipsUrls.add(starshipUrl);
                Starship starship = restTemplate.getForObject(starshipUrl, Starship.class);
                starships.add(starship);
            }
        }

        return starships;
    }
}
