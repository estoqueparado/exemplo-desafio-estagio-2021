package com.estoqueparado.desafioestagio.models;

import java.util.List;

public class PeoplePageResult {

    private String next;

    private List<People> results;

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public List<People> getResults() {
        return results;
    }

    public void setResults(List<People> results) {
        this.results = results;
    }
}
