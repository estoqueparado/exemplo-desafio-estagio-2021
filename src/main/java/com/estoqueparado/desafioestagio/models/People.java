package com.estoqueparado.desafioestagio.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class People {

    private String name;

    @JsonProperty("birth_year")
    private String birthYear;

    private List<String> films;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public List<String> getFilms() {
        return films;
    }

    public void setFilms(List<String> films) {
        this.films = films;
    }
}
