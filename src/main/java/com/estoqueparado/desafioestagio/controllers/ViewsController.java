package com.estoqueparado.desafioestagio.controllers;

import com.estoqueparado.desafioestagio.services.SWAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class ViewsController {

    @Autowired
    private SWAPIService service;

    @GetMapping("/")
    public String index(){
        return "index";
    }

    @GetMapping("/exercise4")
    public String exercise4(Model model){
        model.addAttribute("people", service.getResultExercise1());
        return "exercise4";
    }

    @GetMapping("/exercise5")
    public String exercise5(Model model){
        model.addAttribute("films", service.getResultExercise2());
        return "exercise5";
    }

    @GetMapping("/exercise6")
    public String exercise6(Model model){
        model.addAttribute("starships", service.getResultExercise3());
        return "exercise6";
    }

}
